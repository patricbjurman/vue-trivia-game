const BASE_URL = 'https://opentdb.com/api.php?amount=';
const GET_CATEGORIES = 'https://opentdb.com/api_category.php';
let URL = '';

export const getCategoriesFromApi = () => {
  return fetch(GET_CATEGORIES)
    .then(response => response.json())
    .then(response => response.trivia_categories)
}

export const setUpGameUrl = (numOfQuestions, id, difficulty) => {
  URL = `${BASE_URL}${numOfQuestions}&category=${id}&difficulty=${difficulty}`;
}

export const getQuestionsFromApi = () => {
  return fetch(URL)
    .then(response => response.json())
    .then(response => response.results)
}

export default {getCategoriesFromApi, setUpGameUrl, getQuestionsFromApi};