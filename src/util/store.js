import VueX from "vuex";
import Vue from "vue";
import { getQuestionsFromApi } from "@/util/api.js";
import { randomizeAnswers } from "@/util/util.js";
import { setUpGameUrl } from "@/util/api.js";
import he from 'he';

Vue.use(VueX);

const store = new VueX.Store({
  state: {
    questions: [],
    questionIndex: 0,
    questionsAsString: [],
    userAnswers: [],
    allAnswers: [],
    correctAnswers: [],
    score: 0,
    category: "",
    difficulty: "",
    numberOfQuestions: 0,
  },
  mutations: {
    setQuestions(state, payload) {
      state.questions = payload;
    },
    setQuestionIndex(state) {
      state.questionIndex += 1;
    },
    setUserAnswer(state, payload) {
      state.userAnswers.push(payload);
    },
    setAllAnswers(state) {
      if (state.questions != undefined) {
        for (let i = 0; i < state.questions.length; i++) {
          let array = [];
          for (
            let j = 0;
            j < state.questions[i].incorrect_answers.length;
            j++
          ) {
            let incorrect = he.decode(state.questions[i].incorrect_answers[j]);
            array.push(incorrect);
          }
          let correct = he.decode(state.questions[i].correct_answer);
          array.push(correct);
          state.allAnswers.push(randomizeAnswers(array));
        }
      }
    },
    setCorrectAnswers(state) {
      if (state.questions != undefined) {
        for (let i = 0; i < state.questions.length; i++) {
          state.correctAnswers.push(state.questions[i].correct_answer);
        }
      }
    },
    setCategory(state, payload) {
      state.category = payload;
    },
    setDifficulty(state, payload) {
      state.difficulty = payload;
    },
    setNumberOfQuestions(state, payload) {
      state.numberOfQuestions = payload;
    },
    calculateScore(state) {
      for (let i = 0; i < state.correctAnswers.length; i++) {
        if (state.userAnswers[i] == state.correctAnswers[i]) {
          state.score += 10;
        }
      }
    },
    resetAll(state) {
      state.questionIndex = 0;
      state.userAnswers = [];
      state.allAnswers = [];
      state.correctAnswers = [];
      state.score = 0;
      state.category = "";
      state.difficulty = "";
      state.numberOfQuestions = 0;
      state.questionsAsString = [];
    },
    playAgain(state) {
      setUpGameUrl(state.numberOfQuestions, state.category, state.difficulty);
      state.questions = [];
      state.questionIndex = 0;
      state.userAnswers = [];
      state.allAnswers = [];
      state.correctAnswers = [];
      state.score = 0;
      state.questionsAsString = [];
    },
    setQuestionsToString(state) {
      for (let i = 0; i < state.questions.length; i++) {
        let question = he.decode(state.questions[i].question);
        state.questionsAsString.push(question);
      }
    }
  },
  actions: {
    async loadGame({ commit }) {
      try {
        const questions = await getQuestionsFromApi();
        commit("setQuestions", questions);
      } catch (error) {
        console.log("error in store.loadGame");
      }
    },
    async setAllAnswers({ commit }) {
      commit("setAllAnswers");
    },
  },
  getters: {
    getQuestionsByIndex: (state) => {
      return state.questions[state.questionIndex];
    },
    getQuestionsAsStringByIndex: (state) => {
      return state.questionsAsString[state.questionIndex];
    },
    getCurrentQuestionIndex: (state) => {
      return state.questionIndex;
    },
    getTotalNumberOfQuestions: (state) => {
      return state.questions.length;
    },
    getAllAnswersByIndex: (state) => {
      return state.allAnswers[state.questionIndex];
    },
    getCorrectAnswersByIndex: (state) => (id) => {
      return state.correctAnswers[id];
    },
    getUserAnswersByIndex: (state) => (id) => {
      return state.userAnswers[id];
    },
    getScore: (state) => {
      return state.score;
    },
    getAllUserAnswers: (state) => {
      return state.userAnswers;
    },
    getQuestionByIndex: (state) => (id) => {
      return state.questionsAsString[id];
    },
  },
});

export default store;
