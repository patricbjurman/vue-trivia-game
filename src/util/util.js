export const randomizeAnswers = (answers) => {
  var currentIndex = answers.length,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    [answers[currentIndex], answers[randomIndex]] = [
      answers[randomIndex],
      answers[currentIndex],
    ];
  }
  return answers;
};

export default { randomizeAnswers };
